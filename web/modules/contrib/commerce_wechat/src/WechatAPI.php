<?php
/**
 * 本类封装了微信支付接口相关操作，使用前需要传入正确的接口配置
 * 虽然本类已经封装了全部操作  但开发者仍然需要理解微信支付返回数据的含义才能进行上层系统的开发
 * 因此本类及其方法均列出了参考文档以供快速查阅
 * 为了保证安全 本类没有采用任何第三方封装件 仅使用腾讯公司官方提供的SDK
 *
 * 微信支付文档： https://pay.weixin.qq.com/wiki/doc/apiv3/index.shtml
 * 微信支付首页： https://pay.weixin.qq.com
 *
 * 开发者: 云客 【云游天下，做客四方】
 * 微信号：indrupal
 * Email: phpworld@qq.com
 * Site: www.indrupal.com
 *
 * 开发公司：未来很美（深圳）科技有限公司
 * Site: www.will-nice.com
 *
 */

namespace Drupal\commerce_wechat;

use Drupal\Core\Logger\LoggerChannelInterface;
use WechatPay\GuzzleMiddleware\Util\PemUtil;
use WechatPay\GuzzleMiddleware\WechatPayMiddleware;
use Drupal\commerce_wechat\Certificate\GetWechatCertificate;
use Drupal\commerce_wechat\Exception\CertificateException;
use GuzzleHttp\Exception\RequestException;
use WechatPay\GuzzleMiddleware\Util\AesUtil;
use Symfony\Component\HttpFoundation\Request;
use WechatPay\GuzzleMiddleware\Auth\CertificateVerifier;
use WechatPay\GuzzleMiddleware\Auth\PrivateKeySigner;

/**
 * 因为腾讯公司可能随时改变规则，故微信支付API不做接口实现
 *
 * @package Drupal\commerce_wechat
 */
class WechatAPI {

  //微信接口配置
  protected $config = [];

  //日志记录器
  protected $logger;

  //HTTP请求客户端
  protected $client;

  public function __construct(array $config, LoggerChannelInterface $logger = NULL) {
    $this->config = $config;
    if ($logger) {
      $this->logger = $logger;
    }
    else {
      $this->logger = \Drupal::logger('commerce_wechat');
    }
    $this->client = $this->getHTTPClient();
  }

  /**
   * 下单付款接口
   *
   * @param $order array 订单数组 详见方法内解释
   *
   * @return  bool | string  失败时返回false 成功时返回字符串：二维码链接、H5支付链接、js代码
   *
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_3_1.shtml  H5下单
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_1.shtml  JSAPI下单
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_4_1.shtml  Native下单
   */
  public function order($order) {
    /**
     * 订单数组如下：
     * $order['order_number']
     * 必选 商户订单号 长度string[6,32] 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
     * $order['description']
     * 必选 商品描述 长度string[1,127]
     * $order['total']
     * 必选 总金额 int 订单总金额，单位为分。
     * $order['currency']
     * 必填，货币代码，人民币为CNY
     * $order['notify_url']
     * 必选 通知地址 string[1,256] 通知URL必须为直接可访问的URL（不能重定向），不允许携带查询串。
     * $order['return_url']
     * 必选 返回链接 付款完成后重定向的链接
     * $order['timeout_express']
     * 必选 超时时间戳 超过该时间交易将无法下单
     * $order['openID']
     * 当是JS支付时必填，用户openId
     */

    $type = static::getUserAgentType();
    /**
     * 默认支付场景有：PC（电脑端网页）、WeChat（微信客户端）、Mobile（移动端网页）
     * 微信支付在一种支付场景下如发生下单未付款，则同一商家订单号后续付款须在原场景下进行，其他场景下会提示201重复下单错误
     * 但本模块不会出现该问题，因为采用支付实体的ID，意味着同一个订单每一次调起付款都是不同的微信商家订单号
     */
    if ($type == 'Mobile') {
      return $this->orderH5($order);
    }
    elseif ($type == 'WeChat') {
      return $this->orderJSAPI($order);
    }
    elseif ($type == 'PC') {
      return $this->orderNative($order);
    }
    else {
      return $this->orderH5($order); //移动优先 其实永远不可能执行到这里
    }

  }

  /**
   * PC端网页支付场景，生成二维码后用微信扫码支付
   *
   * @param $order array 见order方法内注释
   *
   * @return bool | string 失败返回false，成功返回二维码支付链接
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_4_1.shtml  Native下单
   */
  public function orderNative($order) {
    $codeURL = NULL;
    $config = $this->config;
    $body = [
      "appid"        => $config['appId'],
      //必选 应用ID
      "mchid"        => $config['merchantId'],
      //必选 商户号 直连商户的商户号，由微信支付生成并下发
      "description"  => $order['description'] ?: 'None',
      //必选 商品描述 长度string[1,127]
      "out_trade_no" => $order['order_number'],
      //必选 商户订单号 长度string[6,32] 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
      "notify_url"   => $order['notify_url'],
      //必选 通知地址 string[1,256] 通知URL必须为直接可访问的URL，不允许携带查询串。如"https://www.weixin.qq.com/wxpay/pay.php"
      "amount"       => [ //必选 订单金额信息
        "total"    => $order['total'], //必选 总金额 int 订单总金额，单位为分。
        "currency" => $order['currency'], //可选 货币类型标识 string[1,16] CNY：人民币，境内商户号仅支持人民币。
      ],

      "time_expire" => date('Y-m-d\TH:i:sP', (int) $order['timeout_express']),
      //可选 交易结束时间 string[1,64] 订单失效时间，遵循rfc3339标准格式 示例值：2018-06-08T10:34:56+08:00
      //见date('Y-m-d\TH:i:sP', time());
      //"attach"      => 'yunke',//可选 附加数据 string[1,128] 在查询API和支付通知中原样返回，可作为自定义参数使用
      //这里我们不要传递附加数据 可能会导致重复下单错误问题。 当用户之前没有支付成功，再次拉起支付时，
      //只有描述、金额、附加数据完全一致，相同订单号才能发起重新支付，否则提示订单重复，因此需要保证每次附加数据相同
    ];

    try {
      $resp = $this->client->request(
        'POST',
        $config['api']['native']['orderURL'] ?: 'https://api.mch.weixin.qq.com/v3/pay/transactions/native', //请求URL
        [
          'json'    => $body,
          'headers' => ['Accept' => 'application/json'],
        ]
      );
      $statusCode = $resp->getStatusCode();
      if ($statusCode == 200) { //处理成功
        $json = json_decode($resp->getBody()->getContents());
        if (!empty($json->code_url)) {
          $codeURL = $json->code_url;
        }
      }
    } catch (RequestException $e) {
      // 进行错误处理
      $msg = "Failed to get QRcode link for wechat PC payment: " . $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= " Status Code:" . $e->getResponse()
            ->getStatusCode() . "; Body:" . $e->getResponse()
            ->getBody() . "\n";
      }
      $this->logger->error($msg);
      return FALSE;
    } catch (\Exception $e) {
      $msg = $e->getMessage();
      $this->logger->error($msg);
      return FALSE;
    }
    if (empty($codeURL)) {
      $msg = 'Failed to get code_url(QRcode link) for wechat PC payment';
      $this->logger->error($msg);
      return FALSE;
    }
    else {
      return $codeURL;
    }
  }

  /**
   * 移动浏览器中的支付，即H5支付
   *
   * @param $order array 见order方法内注释
   *
   * @return bool|string|null 成功时返回H5支付跳转链接，失败时返回false
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_3_1.shtml  H5下单
   */
  public function orderH5($order) {
    $h5URL = NULL; //H5支付链接
    $config = $this->config;
    // 统一下单JSON请求体 参数见：https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_3_1.shtml
    $body = [
      "appid"        => $config['appId'],
      //必选 应用ID
      "mchid"        => $config['merchantId'],
      //必选 商户号 直连商户的商户号，由微信支付生成并下发
      "description"  => $order['description'] ?: 'None',
      //必选 商品描述 长度string[1,127]
      "out_trade_no" => $order['order_number'],
      //必选 商户订单号 长度string[6,32] 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
      "notify_url"   => $order['notify_url'],
      //必选 通知地址 string[1,256] 通知URL必须为直接可访问的URL，不允许携带查询串。如"https://www.weixin.qq.com/wxpay/pay.php"
      "amount"       => [ //必选 订单金额信息
        "total"    => $order['total'], //必选 总金额 int 订单总金额，单位为分。
        "currency" => $order['currency'], //可选 货币类型标识 string[1,16] CNY：人民币，境内商户号仅支持人民币。
      ],
      "scene_info"   => [ //必选 支付场景描述
        "payer_client_ip" => \Drupal::request()->getClientIp(),
        //必选 用户终端IP string[1,45] 用户的客户端IP，支持IPv4和IPv6两种格式的IP地址。
        "h5_info"         => [// 必选 H5场景信息
          "type" => "Wap",
          //必选 场景类型 string[1,32] 场景类型 示例值：iOS, Android, Wap
        ],
      ],

      "time_expire" => date('Y-m-d\TH:i:sP', (int) $order['timeout_express']),
      //可选 交易结束时间 string[1,64] 订单失效时间，遵循rfc3339标准格式 示例值：2018-06-08T10:34:56+08:00
      //见date('Y-m-d\TH:i:sP', time());
    ];
    $msg = NULL; //错误消息
    try {
      $resp = $this->client->request(
        'POST',
        $config['api']['h5']['orderURL'] ?: 'https://api.mch.weixin.qq.com/v3/pay/transactions/h5', //请求URL
        [
          'json'    => $body,
          'headers' => ['Accept' => 'application/json'],
        ]
      );
      $statusCode = $resp->getStatusCode();
      if ($statusCode == 200) { //处理成功
        $json = json_decode($resp->getBody()->getContents());
        if (!empty($json->h5_url)) {
          $h5URL = $json->h5_url . '&redirect_url=' . urlencode($order['return_url']);
        }
        else {
          $msg = 'H5 redirect url does not exist in wechat H5 payment';
        }
      }
      else {
        $msg = 'Response error in wechat H5 payment, Status Code:' . $statusCode;
      }
    } catch (RequestException $e) {
      // 进行错误处理
      $msg = 'Failed to get H5 url in wechat H5 payment,' . $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "Status Code:" . $e->getResponse()
            ->getStatusCode() . "; Body:" . $e->getResponse()
            ->getBody() . "\n";
      }
      $this->logger->error($msg);
    } catch (\Exception $e) {
      $msg = $e->getMessage();
      $this->logger->error($msg);
    }
    if (!empty($h5URL)) {
      return $h5URL;
    }
    $this->logger->error($msg);
    return FALSE;
  }


  /**
   * 在微信浏览器内部支付 通过JS调用发起付款
   *
   * @param $order array 见order方法内注释
   *
   * @return bool|string 失败时返回false,成功时返回JS执行代码
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_1.shtml  JSAPI下单
   */
  public function orderJSAPI($order) {
    //注意调用该方法时，必须提前获取到openid，并在$order['openID']中传入
    $prepayID = NULL; //预支付id
    $body = [
      "appid"        => $this->config['appId'],
      //必选 应用ID
      "mchid"        => $this->config['merchantId'],
      //必选 商户号 直连商户的商户号，由微信支付生成并下发
      "description"  => $order['description'] ?: 'None',
      //必选 商品描述 长度string[1,127]
      "out_trade_no" => $order['order_number'],
      //必选 商户订单号 长度string[6,32] 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
      "notify_url"   => $order['notify_url'],
      //必选 通知地址 string[1,256] 通知URL必须为直接可访问的URL，不允许携带查询串。如"https://www.weixin.qq.com/wxpay/pay.php"
      "amount"       => [ //必选 订单金额信息
        "total"    => $order['total'], //必选 总金额 int 订单总金额，单位为分。
        "currency" => $order['currency'], //可选 货币类型标识 string[1,16] CNY：人民币，境内商户号仅支持人民币。
      ],
      "payer"        => [
        "openid" => $order['openID'],
        //微信浏览器内下单必须要用户openId,类似："oUpF8uMuAJO_M2pxb1Q9zNjWeS6o"，
        //云客个人觉得完全没必要，为系统设计带来不优雅，腾讯的这种做法是为了强行将用户拉入其生态系统，难以接受
      ],

      "time_expire" => date('Y-m-d\TH:i:sP', (int) $order['timeout_express']),
      //可选 交易结束时间 string[1,64] 订单失效时间，遵循rfc3339标准格式 示例值：2018-06-08T10:34:56+08:00
      //见date('Y-m-d\TH:i:sP', time());
    ];

    try {
      $resp = $this->client->request(
        'POST',
        $this->config['api']['js']['orderURL'] ?: 'https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi', //请求URL
        [
          'json'    => $body,
          'headers' => ['Accept' => 'application/json'],
        ]
      );
      $statusCode = $resp->getStatusCode();
      if ($statusCode == 200) { //处理成功
        $json = json_decode($resp->getBody()->getContents());
        if (!empty($json->prepay_id)) {
          $prepayID = $json->prepay_id;
        }
      }
    } catch (RequestException $e) {
      // 进行错误处理
      $msg = 'Failed to create wechat prepayID';
      $msg .= $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "Status Code:" . $e->getResponse()
            ->getStatusCode() . "; Body:" . $e->getResponse()
            ->getBody() . "\n";
      }
      $this->logger->error($msg);
      return FALSE;
    } catch (\Exception $e) {
      $msg = $e->getMessage();
      $this->logger->error($msg);
      return FALSE;
    }

    if (empty($prepayID)) {
      $msg = 'Failed to create wechat prepayID';
      $this->logger->error($msg);
      return FALSE;
    }
    //预支付创建成功，接下来返回前端js代码调起支付
    return $this->getJavaScript($prepayID, $order['return_url']);
  }

  /**
   * 返回调起微信支付的js代码
   *
   * @param $prepayId  string 预下单ID
   * @param $returnURL string 付款后的返回链接
   *
   * @return string 在页面中执行的调起支付的JS代码
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/open/pay/chapter2_3.shtml#part-6
   */
  protected function getJavaScript($prepayId, $returnURL) {
    $appId = $this->config['appId'];
    $timeStamp = time();
    $nonceStr = $this->getRandStr(32);
    $package = "prepay_id={$prepayId}";
    $paySign = $this->getSign("$appId\n$timeStamp\n$nonceStr\n$package\n");

    $js = <<<yunke
   <script type="text/javascript">
    function onBridgeReady() {
        WeixinJSBridge.invoke('getBrandWCPayRequest', {
                "appId": "{$appId}",   //公众号ID，由商户传入
                "timeStamp": "{$timeStamp}",   //时间戳，自1970年以来的秒数
                "nonceStr": "{$nonceStr}",      //随机串
                "package": "{$package}",
                "signType": "RSA",     //微信签名方式：
                "paySign": "{$paySign}" //微信签名
            },
            function (res) {
                //if (res.err_msg == "get_brand_wcpay_request:ok") {
                    // 使用以上方式判断前端返回,微信团队郑重提示：
                    //res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
                    location.href="{$returnURL}";
                //}
            });
    }

    if (typeof WeixinJSBridge == "undefined") {
        if (document.addEventListener) {
            document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
        } else if (document.attachEvent) {
            document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
            document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
        }
    } else {
        onBridgeReady();
    }
   </script>
yunke;

    return $js;
  }

  /**
   * 得到接口加签随机字符串 用于JS支付
   *
   * @param $len
   *
   * @return string
   */
  private function getRandStr($len) {
    //随机字符串
    $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
    $nonce = '';
    $max = strlen($str) - 1;
    for ($i = 0; $i < $len; $i++) {
      $nonce .= $str[mt_rand(0, $max)];
    }
    return $nonce;
  }

  /**
   * 通过商户订单号查询订单状态
   *
   * @param $outTradeNo string 商户系统中保存的商户订单号  并非微信订单号 即经过转化的支付实体ID
   *
   * @return array|bool 查询失败时返回false 否则返回信息数组（但并不表示业务成功，当订单不存在时也返回信息数组）
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_2.shtml
   */
  public function query($outTradeNo) {
    $url = $this->config['api']['queryURL'] ?: 'https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/';
    $url = $url . $outTradeNo . '?mchid=' . $this->config['merchantId'];
    try {
      $resp = $this->client->request(
        'GET',
        $url, //请求URL
        [
          'headers' => ['Accept' => 'application/json'],
        ]
      );
      $statusCode = $resp->getStatusCode();
      if ($statusCode == 200) { //处理成功
        $json = json_decode($resp->getBody()->getContents(), TRUE);
        return $json;
      }
      else {
        $msg = 'Payment query has an error, status code:' . $statusCode;
        $this->logger->error($msg);
        return FALSE;
      }
    } catch (RequestException $e) {
      // 进行错误处理
      $msg = "Failed to query order payment:{$outTradeNo}, ";
      $msg .= $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "Status Code:" . $e->getResponse()->getStatusCode() . " return body:" . $e->getResponse()->getBody() . "\n";
      }
      $this->logger->error($msg);
      return FALSE;
    }
  }

  /**
   * 订单退款接口
   *
   * @param $order array 退款参数数组 详见方法内说明
   *
   * @return array|bool 成功返回信息数组 失败返回false
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_9.shtml
   */
  public function refund($order) {
    /**
     * $order['order_number']
     * 必选 商户订单号 长度string[6,32] 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
     * $order['refund_number']
     * 必选 退款单号  String  最大长度64
     * $order['refund_amount']
     * 必选 退款金额 单位分
     * $order['total']
     * 必选 原订单总金额 单位分
     * $order['currency']
     * 必填，货币代码，人民币为CNY
     * $order['refund_reason']
     * 可选 退款原因 String  最大长度256
     * $order['notify_url']
     * 可选 退款异步通知地址 string[1,256] 通知URL必须为直接可访问的URL，不允许携带查询串。
     */
    $body = [
      "out_trade_no"  => $order['order_number'],
      "out_refund_no" => $order['refund_number'],
      "amount"        => [ //必选 退款金额信息
        "refund"   => (int) $order['refund_amount'], //必选 退款金额 int，单位为分。
        "total"    => (int) $order['total'], //必选 退款金额 int，单位为分。
        "currency" => $order['currency'], //可选 货币类型标识 string[1,16] CNY：人民币，境内商户号仅支持人民币。
      ],
    ];
    if (!empty($order['refund_reason'])) {
      $body["reason"] = $order['refund_reason'];
    }
    if (!empty($order['notify_url'])) {
      $body["notify_url"] = $order['notify_url'];
    }

    $msg = NULL; //错误消息
    try {
      $resp = $this->client->request(
        'POST',
        $this->config['api']['refundURL'] ?: 'https://api.mch.weixin.qq.com/v3/refund/domestic/refunds', //请求URL
        [
          'json'    => $body,
          'headers' => ['Accept' => 'application/json'],
        ]
      );
      $statusCode = $resp->getStatusCode();
      if ($statusCode == 200) { //处理成功
        $json = json_decode($resp->getBody()->getContents(), TRUE);
        return $json;
      }
      else {
        return FALSE;
      }
    } catch (RequestException $e) {
      // 进行错误处理
      $msg = 'Wechat Pay failed to refund' . $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "Status Code:" . $e->getResponse()
            ->getStatusCode() . "; Body:" . $e->getResponse()
            ->getBody() . "\n";
      }
      $this->logger->error($msg);
      return FALSE;
    }
  }

  /**
   * 订单退款查询
   *
   * @param $outRefundNo string 商户系统内部的退款单号
   *
   * @return array|bool 查询成功返回信息数组 失败返回false
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_4_10.shtml
   */
  public function queryRefund($outRefundNo) {
    $url = $this->config['api']['queryRefundURL'] ?: 'https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/';
    $url = $url . $outRefundNo;
    try {
      $resp = $this->client->request(
        'GET',
        $url, //请求URL
        [
          'headers' => ['Accept' => 'application/json'],
        ]
      );
      $statusCode = $resp->getStatusCode();
      if ($statusCode == 200) { //处理成功
        return json_decode($resp->getBody()->getContents(), TRUE);
      }
      else {
        return FALSE;
      }
    } catch (RequestException $e) {
      // 进行错误处理
      $msg = 'Failed to query order refund';
      $msg .= $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "Status Code:" . $e->getResponse()->getStatusCode() . " return body:" . $e->getResponse()->getBody() . "\n";
      }
      $this->logger->warning($msg);
      return FALSE;
    }
  }


  /**
   * 获取微信支付平台证书
   *
   * @param array                                           $config 微信API接口配置
   * @param \Drupal\Core\Logger\LoggerChannelInterface|NULL $logger 日志服务
   *
   * @return array|bool 获取成功时返回数组，失败时返回False
   */
  static public function getWechatCertificate(array $config, LoggerChannelInterface $logger = NULL) {
    try {
      return (new GetWechatCertificate($config, $logger))->getCertificate();
    } catch (CertificateException $e) {
      return FALSE;
    }
  }

  /**
   * 得到连接微信服务器的http客户端
   * 其中已经压入了中间件  会自动加签和验签等
   *
   * @return \GuzzleHttp\Client
   *
   * @throws \Exception
   * @see https://github.com/wechatpay-apiv3/wechatpay-guzzle-middleware
   * @see https://github.com/wechatpay-apiv3/wechatpay-php
   */
  public function getHTTPClient() {
    static $HTTPClient = NULL;
    if ($HTTPClient !== NULL) {
      return $HTTPClient;
    }
    $wechatConfig = $this->config;
    $wechatPayCertificate = $wechatConfig['wechatPayCertificate'];
    if (!$wechatPayCertificate) {
      $msg = (string) t('Can not get Wechat pay HTTPClient, Missing wechat platform certificate');
      $this->logger->error($msg);
      throw new \Exception($msg);
    }

    // 商户相关配置
    $merchantId = $wechatConfig['merchantId']; // 商户号
    $merchantSerialNumber = $wechatConfig['merchantSerialNumber']; // 商户API证书序列号
    $merchantPrivateKey = PemUtil::loadPrivateKeyFromString($wechatConfig['merchantPrivateKey']); // 商户私钥

    // 微信支付平台配置
    $wechatPayCertificates = [];
    foreach ($wechatPayCertificate as $certificate) {
      $wechatPayCertificates[] = PemUtil::loadCertificateFromString($certificate['certificate']);// 微信支付平台证书
    }

    // 构造一个WechatPayMiddleware
    $wechatPayMiddleware = WechatPayMiddleware::builder()
      ->withMerchant($merchantId, $merchantSerialNumber, $merchantPrivateKey) // 传入商户相关配置
      ->withWechatPay($wechatPayCertificates) // 可传入多个微信支付平台证书，参数类型为array
      ->build();

    $client = \Drupal::service('http_client_factory')->fromOptions();
    $client->getConfig('handler')->push($wechatPayMiddleware, 'commerce_wechat');

    $HTTPClient = $client;
    return $HTTPClient;
  }

  /**
   * 判断客户端属于何种类型的支付场景 返回三种情况（有且仅有这三种）：
   * PC（电脑端网页）、WeChat（微信客户端）、Mobile（移动端网页）
   * 以此应用不同的支付方式，该方法为主流方式的精简版，如果设备发送了不正确的代理标识，会导致无法付款
   *
   * @param null $userAgent 用户代理标识
   *
   * @return string PC|WeChat|Mobile
   */
  static public function getUserAgentType($userAgent = NULL) {
    //常见用户代理：
    //火狐PC： Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0
    //谷歌PC： Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
    //微软PC： Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.62
    //微信PC： Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36 QBCore/4.0.1326.400 QQBrowser/9.0.2524.400 Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2875.116 Safari/537.36 NetType/WIFI MicroMessenger/7.0.20.1781(0x6700143B) WindowsWechat(0x63010200)


    //火狐移动：Mozilla/5.0 (Android 9; Mobile; rv:88.0) Gecko/88.0 Firefox/88.0
    //谷歌移动：
    //微软移动：Mozilla/5.0 (Linux; Android 9; MI 6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.116 Mobile Safari/537.36 EdgA/45.09.4.5079
    //微信移动：Mozilla/5.0 (Linux; Android 9; MI 6 Build/PKQ1.190118.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/78.0.3904.62 XWEB/2797 MMWEBSDK/20210302 Mobile Safari/537.36 MMWEBID/1758 MicroMessenger/8.0.3.1880(0x28000339) Process/toolsmp WeChat/arm64 Weixin NetType/WIFI Language/zh_CN ABI/arm64
    //小米移动：Mozilla/5.0 (Linux; U; Android 9; zh-cn; MI 6 Build/PKQ1.190118.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/79.0.3945.147 Mobile Safari/537.36 XiaoMi/MiuiBrowser/14.4.18
    //QQ移动：  Mozilla/5.0 (Linux; U; Android 9; zh-cn; MI 6 Build/PKQ1.190118.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.120 MQQBrowser/11.5 Mobile Safari/537.36 COVC/045429
    //夸克移动：Mozilla/5.0 (Linux; U; Android 9; zh-CN; MI 6 Build/PKQ1.190118.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/78.0.3904.108 Quark/4.9.0.176 Mobile Safari/537.36


    $userAgentType = 'PC'; //PC Mobile WeChat
    $userAgent = $userAgent ?: $_SERVER['HTTP_USER_AGENT'];
    if (stripos($userAgent, 'WeChat') !== FALSE && stripos($userAgent, 'Mobile') !== FALSE) {
      $userAgentType = 'WeChat';
    }
    elseif (stripos($userAgent, 'MicroMessenger') !== FALSE && stripos($userAgent, 'Mobile') !== FALSE) {
      $userAgentType = 'WeChat';
    }
    elseif (stripos($userAgent, 'Mobile') !== FALSE) {
      $userAgentType = 'Mobile';
    }
    return $userAgentType;
  }

  /**
   * 得到发送给微信的消息的签名值
   *
   * @param $message string 消息内容 参见本类的getJavaScript方法
   *
   * @return string 消息签名值
   */
  public function getSign($message) {
    $merchantSerialNumber = $this->config['merchantSerialNumber']; // 商户API证书序列号
    $merchantPrivateKey = PemUtil::loadPrivateKeyFromString($this->config['merchantPrivateKey']); // 商户私钥
    $signer = new PrivateKeySigner($merchantSerialNumber, $merchantPrivateKey);
    $signResult = $signer->sign($message);
    $sign = $signResult->getSign();
    return $sign;
  }

  /**
   * 订单付款、退款的异步通知验签
   *
   * @param \Symfony\Component\HttpFoundation\Request $request 来自\Drupal::request();
   *
   * @return bool 是否成功 true为验签成功 说明请求来自微信服务器  否则应该放弃处理
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_5.shtml
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_11.shtml
   */
  public function verifyNotify(Request $request) {
    $serialNo = $request->headers->get('Wechatpay-Serial');
    $sign = $request->headers->get('Wechatpay-Signature');
    $timestamp = $request->headers->get('Wechatpay-TimeStamp');
    $nonce = $request->headers->get('Wechatpay-Nonce');

    if (!isset($serialNo, $sign, $timestamp, $nonce)) {
      return FALSE;
    }
    if (!(\abs((int) $timestamp - \time()) <= 300)) { //时间差错超过5分钟的通知被放弃
      return FALSE;
    }
    $body = $request->getContent();
    $message = "$timestamp\n$nonce\n$body\n";
    $wechatPayCertificate = $this->config['wechatPayCertificate'];
    // 微信支付平台公钥配置
    $wechatPayCertificates = [];
    foreach ($wechatPayCertificate as $certificate) {
      $wechatPayCertificates[] = PemUtil::loadCertificateFromString($certificate['certificate']);// 微信支付平台证书
    }
    $validator = new CertificateVerifier($wechatPayCertificates);
    try {
      $result = $validator->verify($serialNo, $message, $sign);
      return $result;
    } catch (\Exception $e) {
      $this->logger->error('Failed to process wechat Pay signature verification, ' . $e->getMessage());
      return FALSE;
    }
  }

  /**
   * 解密出付款、退款异步通知的内容
   *
   * @param \Symfony\Component\HttpFoundation\Request $request 来自\Drupal::request();
   *
   * @return array|bool 解密成功时返回信息数组 否则返回false
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_5.shtml
   * @see https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_11.shtml
   */
  public function decrypt(Request $request) {
    $body = json_decode($request->getContent());
    if (isset($body->resource->ciphertext)) {//解密出通知的json数据
      $key = $this->config['apiV3SecretKey'];
      $aes = new AesUtil($key);
      $associatedData = $body->resource->associated_data;
      $nonceStr = $body->resource->nonce;
      $ciphertext = $body->resource->ciphertext;
      $result = $aes->decryptToString($associatedData, $nonceStr, $ciphertext);
      $result = json_decode($result, TRUE);
      return $result;
    }
    else {
      return FALSE;
    }
  }

  /**
   * 在微信支付的JSAPI中（也就是在微信浏览器中支付时），下单要求有用户的openid
   * 而这需要做一个用户授权操作才能拿到，该方法即是用来返回授权链接的
   *
   * @param $redirectURL string 授权后的重定向链接，即返回地址
   *
   * @return string  授权URL链接
   */
  public function getOauth2URL($redirectURL) {
    $appid = $this->config['appId'];
    $redirectURL = \urlencode($redirectURL);
    $oauth2URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" . $appid . "&redirect_uri=" . $redirectURL . "&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";
    return $oauth2URL;
  }

  /**
   * 用授权码code去取回用户的OpenId
   *
   * @param $code string 授权码
   *
   * @return bool | string 失败时返回false,成功时返回字符串openId
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getOpenId($code) {
    $appid = $this->config['appId'];
    $secret = $this->config['appSecretKey'];
    $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" . $appid . "&secret=" . $secret . "&code=" . $code . "&grant_type=authorization_code";
    try {
      $resp = \Drupal::httpClient()->request( //这属于公众号功能，不用微信支付客户端对象
        'GET',
        $url,
        [
          'headers' => ['Accept' => 'application/json'],
        ]
      );
      $statusCode = $resp->getStatusCode(); //return $statusCode.''.$resp->getBody()->getContents();
      if ($statusCode == 200) { //处理成功
        $json = json_decode($resp->getBody()->getContents());
        if (!empty($json->openid)) {
          return $json->openid;
        }
        else {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    } catch (RequestException $e) {
      // 进行错误处理
      $msg = 'Failed to get wechat openID, ';
      $msg .= $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "Status Code:" . $e->getResponse()
            ->getStatusCode() . "; Body:" . $e->getResponse()
            ->getBody() . "\n";
      }
      $this->logger->error($msg);
      return FALSE;
    }
  }

  /**
   * 得到会话对象 用于保存用户的openid等
   *
   * @return \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  static public function getSession() {
    $request = \Drupal::request();
    if (!$request->hasSession()) {
      //通常不会执行这里，在系统初期http堆栈阶段，服务http_middleware.session会启动会话
      //但预防其他模块干扰，此处以备会话丢失，确保验证器以此得到会话：$session = $request->getSession();
      $session = \Drupal::service('session');
      $session->start();
      $request->setSession($session);
    }
    return $request->getSession();
  }

}

<?php
/**
 * 构建支付页面
 */

namespace Drupal\commerce_wechat\Form;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_wechat\WechatAPI;
use Drupal\commerce_wechat\OrderIDConverter;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Render\Markup;

class PaymentForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /**
     * 该方法在以下方法中被调用：
     * \Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentProcess::buildPaneForm
     * 本类$this->entity为新建的支付实体（Drupal\commerce_payment\Entity\Payment）
     * 但尚未保存，由于微信支付的特点，它的bundle适合设置为payment_manual
     * 返回链接$form['#return_url']已经被设置为路由：commerce_payment.checkout.return，值为绝对路径字符串，如：
     * 'http://shop.dp.com/checkout/22/payment/return'
     * 取消链接$form['#cancel_url']已经被设置为路由：commerce_payment.checkout.cancel，值为绝对路径字符串，如：
     * 'http://shop.dp.com/checkout/22/payment/cancel'
     * 异步通知路由为：commerce_payment.notify，须在此手动设置，地址如下：
     * /payment/notify/PAYMENT_GATEWAY_ID
     */

    $form = parent::buildConfigurationForm($form, $form_state);
    $payment = $this->entity; //新建的支付实体，尚未保存
    $gatewayEntity = $payment->getPaymentGateway(); //支付网关配置实体
    $gatewayPlugin = $gatewayEntity->getPlugin(); //支付网关插件对象
    $config = $gatewayEntity->getPluginConfiguration();//插件配置，即微信API接口配置
    $orderEntity = $payment->getOrder(); //订单实体
    $this->logger = \Drupal::logger('commerce_wechat');

    $userAgentType = WechatAPI::getUserAgentType();//仅三种可能值：PC|WeChat|Mobile
    $wechatAPI = new WechatAPI($config, $this->logger);

    if ($userAgentType == 'WeChat') {
      //由于微信内部浏览器发起的支付必须要用户的OpenID ,获取OpenID需要进行一次授权跳转，因此先行处理
      //先查询OpenID是否存在，如果没有则跳转到授权链接，授权后会拿到code码，以此去换OpenID，最后再进行支付
      $session = WechatAPI::getSession();
      $wechatOpenID = $session->get('commerce_wechat_wechatOpenID', NULL);
      if (empty($wechatOpenID)) {
        $request = \Drupal::request();
        $code = $request->query->get('code');
        if (empty($code)) {//判断是否有授权码，如果有，说明已经执行过跳转授权并返回来了，否则执行授权操作
          $oauth2URL = $wechatAPI->getOauth2URL($request->getUri()); //得到获取openid的授权链接,授权后再次返回这里
          throw new NeedsRedirectException($oauth2URL); //跳转到授权页面
          /**
           * 经测试，当授权出现故障时（比如微信管理后台授权域名配置不匹配），微信页面将终止停留在前一页，仅以弹框提示错误
           * 这种情况下没有自动返回，因此这里不会出现反复重定向到授权页的死循环。授权成功时返回参数除code外还有state参数
           */
        }
        else {
          $wechatOpenID = $wechatAPI->getOpenId($code);
          if ($wechatOpenID === FALSE) {
            \Drupal::messenger()->addError($this->t('Unable to complete payment, Failed to get wechat openID'));
            throw new PaymentGatewayException('Unable to complete payment, Failed to get wechat openID');
          }
          else {
            $session->set('commerce_wechat_wechatOpenID', $wechatOpenID);
          }
        }
      }
    }

    /**
     * 我们以系统ID和支付实体的ID做微信商户订单ID，因此必须先保存，如果后续出现各种原因终断的支付，那么这些被保存的支付实体
     * 会成为系统垃圾，不过没关系，长时间弃用的未付款支付实体会被回收机制清理，详见计划任务
     */
    $expiresTime = time() + $config['expires'];
    $payment->expires = $expiresTime;
    $payment->save();

    // 构建订单数组如下：
    $order['order_number'] = OrderIDConverter::toWechat($payment->id(), $config['systemId']);
    // 必选 商户订单号 长度string[6,32] 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
    //以第一个条目对象的标题作为订单描述,截取到固定长度
    $description = '';
    if (isset($orderEntity->getItems()[0])) {
      $description = Unicode::truncate($orderEntity->getItems()[0]->getTitle(), 20, FALSE, TRUE);
    }
    $order['description'] = $description . $this->t('OrderID') . ':' . $orderEntity->id();//用ID是否会导致商业机密泄露，请自行更改
    // 必选 商品描述 长度string[1,127]
    $order['total'] = \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($payment->getAmount());
    //必选 总金额 int 订单总金额，单位为分。
    $order['currency'] = $payment->getAmount()->getCurrencyCode();
    //必选 货币单位 注意微信支付境内商户号仅支持人民币：CNY
    $order['notify_url'] = $gatewayPlugin->getNotifyUrl()->toString(FALSE);
    // 必选 通知地址 string[1,256] 通知URL必须为直接可访问的URL（不能重定向），不允许携带查询串。
    $order['return_url'] = $form['#return_url'];
    //必选 返回链接 付款完成后重定向的链接
    $order['timeout_express'] = $expiresTime;
    //必选 超时时间戳 超过该时间交易将无法下单 默认一个月

    $data = ['payment' => $payment->id()];
    //点击完成后传递给返回链接的数据

    if ($userAgentType === 'PC') {
      $codeURL = $wechatAPI->orderNative($order);
      if ($codeURL == FALSE) {
        \Drupal::messenger()->addError($this->t('Wechat Pay has an error, To contact the administrator'));
        throw new NeedsRedirectException($form['#cancel_url']);
      }
      $form['pay_info'] = [
        '#type'        => 'details',
        '#title'       => $this->t('Wechat scan QRcode to pay'),
        '#open'        => TRUE,
        '#attributes'  => [
          'class' => ['commerce-wechat-qrcode'], //提供一个定位点给主题开发者使用
        ],
        '#description' => $this->t('after payment, Click on button to continue...'),//'扫码付款后点击按钮继续'
      ];
      $form['pay_info']['qrcode'] = [
        '#type'       => 'yunke_qrcode',
        '#text'       => $codeURL,
        '#foreground' => $config['qrcolor'],
        '#background' => $config['qrbgcolor'],
        '#width'      => $config['qrsize'],
        '#height'     => $config['qrsize'],
      ];
      foreach ($data as $key => $value) {
        $form[$key] = [
          '#type'    => 'hidden',
          '#value'   => $value,
          '#parents' => [$key],
        ];
      }
      $form['#process'][] = [get_class($this), 'processRedirectForm'];
      $form['#redirect_url'] = $order['return_url'];
      return $form;
    }
    elseif ($userAgentType === 'Mobile') {
      $order['return_url'] = Url::fromUri($order['return_url'], ['absolute' => TRUE, 'query' => $data])->toString();
      $h5URL = $wechatAPI->orderH5($order);
      if ($h5URL == FALSE) {
        \Drupal::messenger()->addError($this->t('Wechat Pay has an error, To contact the administrator'));
        throw new NeedsRedirectException($form['#cancel_url']);
      }
      throw new NeedsRedirectException($h5URL);
    }
    elseif ($userAgentType === 'WeChat') {
      $order['return_url'] = Url::fromUri($order['return_url'], ['absolute' => TRUE, 'query' => $data])->toString();
      $order['openID'] = $wechatOpenID;
      $JScode = $wechatAPI->orderJSAPI($order);
      if ($JScode == FALSE) {
        \Drupal::messenger()->addError($this->t('Wechat Pay has an error, To contact the administrator'));
        throw new PaymentGatewayException('Unable to complete payment, Failed to call wechat JSAPI');
      }
      $form['JScode'] = [
        '#markup' => Markup::create($JScode),
      ];
      $msg = $this->t('Pay waiting... ') . '<a href="' . $order['return_url'] . '" class="button commerce-wechat-cancel-link">' . $this->t('Cancel') . '</a>';
      $form['msg'] = [
        '#markup' => Markup::create($msg),
      ];
      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function processRedirectForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    $form = parent::processRedirectForm($form, $form_state, $complete_form);
    $complete_form['actions']['next']['#value'] = t('Payment completed');
    $complete_form['#method'] = 'GET';
    return $form;
  }

}

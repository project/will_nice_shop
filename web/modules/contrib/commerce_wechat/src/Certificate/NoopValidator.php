<?php

/**
 *  临时跳过签名用的验证器
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\commerce_wechat\Certificate;

use WechatPay\GuzzleMiddleware\Validator;
use Psr\Http\Message\ResponseInterface;


class NoopValidator implements Validator
{
  public function validate(ResponseInterface $response)
  {
    return true;
  }
}

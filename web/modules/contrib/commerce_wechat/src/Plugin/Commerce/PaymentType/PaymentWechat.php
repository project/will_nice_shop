<?php

namespace Drupal\commerce_wechat\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
/**
 * Provides the wechat payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_wechat",
 *   label = @Translation("Wechat"),
 *   workflow = "payment_wechat",
 * )
 */
class PaymentWechat extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}

<?php

namespace Drupal\commerce_wechat\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_wechat\WechatAPI;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_wechat\OrderIDConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides the Wechat payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "wechat_pay",
 *   label = @Translation("wechat pay"),
 *   display_label = @Translation("wechat pay"),
 *   modes = {
 *     "n/a" = @Translation("N/A"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_wechat\Form\PaymentForm",
 *     "refund-payment"  = "Drupal\commerce_wechat\Form\PaymentRefundForm",
 *     "view-refund"  = "Drupal\commerce_wechat\Form\ListRefundForm",
 *   },
 *   payment_type = "payment_wechat",
 *   requires_billing_information = FALSE,
 * )
 */
class Wechat extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {

  //退款状态常量
  const REFUND_NEW = 'NEW';//退款新建

  const REFUND_SUCCESS = 'SUCCESS';//退款成功

  const REFUND_FAIL = 'FAIL';//退款失败

  //日志服务 默认为：\Drupal::logger('commerce_wechat');
  protected $logger = NULL;

  //默认配置
  public function defaultConfiguration() {
    $defaultConfiguration = [
      //系统配置
      //是否收集客户账单信息
      'collect_billing_information' => FALSE,
      //PC端收款二维码前景色
      'qrcolor'                     => '#33CC99',
      //PC端收款二维码背景色
      'qrbgcolor'                   => '#FFFFFF',
      //PC端收款二维码尺寸
      'qrsize'                      => 256,
      //系统ID,在同一套微信接口账号下，如果安装了多套本系统，可能出现订单重复而无法付款，因此以此区分不同系统的订单
      'systemId'                    => 'yunke',
      //支付超期时间，超过该时间间隔将不能发起支付，超过该期限且未支付的支付实体将被系统净化删除
      'expires'                     => 2592000,//默认为30天，30*24*60*60

      //app ID 默认为'' 如：wxafc48c7edb5a9a1f
      'appId'                       => '',

      //app密钥 仅在微信浏览器内调用jsapi下单时才用到，通常是微信公众号开发者密钥 如：eccfef464987e8abadd42750c889d373
      'appSecretKey'                => '',

      //微信支付商户号 默认为null 如：1608351645
      'merchantId'                  => '',

      //api密钥 如：h0olu7564hpcj6w3szk86oyhserewt47
      'apiSecretKey'                => '',

      //APIv3密钥 如：hfd65gq4fd4hd7ljd4a54r5ax4jrd3sr
      'apiV3SecretKey'              => '',

      //商户API证书序列号 如：1EE294649016ECA78E275FCF32229D0719FBF120
      'merchantSerialNumber'        => '',

      //保存商户私钥，即下载的私钥文件apiclient_key.pem的内容
      'merchantPrivateKey'          => '',

      //微信支付平台证书，数组格式,每个元素代表一个证书（平台可能同时使用多个证书）
      //每个证书元素数组有如下键：serial_no、effective_time、expire_time、certificate
      //分别代表证书序列号、生效日期、失效日期、证书内容字符串
      //详见 \Drupal\commerce_wechat\WechatAPI::getWechatCertificate
      'wechatPayCertificate'        => NULL,

      //微信支付平台证书获得时间,超过12小时将重新获取平台证书,默认为0，正常值是时间戳
      'wechatPayCertificateTime'    => 0,

      //API接口信息，如请求地址等
      'api'                         => [
        'certificatesURL' => 'https://api.mch.weixin.qq.com/v3/certificates',
        //平台证书获取接口

        'native' => [
          'orderURL' => 'https://api.mch.weixin.qq.com/v3/pay/transactions/native',
          //统一下单API URL
        ],

        'h5' => [
          'orderURL' => 'https://api.mch.weixin.qq.com/v3/pay/transactions/h5',
          //h5统一下单API URL
        ],

        'js' => [
          'orderURL' => 'https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi',
          //微信客户端js统一下单API URL
        ],

        'queryURL' => 'https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/',
        //订单查询接口,使用商户订单号查询

        'refundURL' => 'https://api.mch.weixin.qq.com/v3/refund/domestic/refunds',
        //订单退款接口

        'queryRefundURL' => 'https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/',
        //查询退款接口 和退款接口相同 但是GET请求
      ],
    ];
    $defaultConfiguration += parent::defaultConfiguration();
    return $defaultConfiguration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['notice'] = [
      '#markup' => $this->t('If you are in trouble and need help, To contact <a href="http://www.indrupal.com" target="_blank">Yunke</a> (Wechat ID: indrupal)'),
    ];
    $form['appId'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('APP ID'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['appId'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['appSecretKey'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('APP secret key'),//API密钥
      '#description'   => $this->t('App developer secret, JSAPI need it to get user\'s openID when paying in the WeChat browser'),
      '#default_value' => $this->configuration['appSecretKey'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['merchantId'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Merchant ID'),//商户号
      '#required'      => TRUE,
      '#default_value' => $this->configuration['merchantId'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['apiSecretKey'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API secret key'),//API密钥
      '#description'   => $this->t('A 32 character string, Set and get it from the WeChat payment platform'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['apiSecretKey'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['apiV3SecretKey'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API v3 secret key'),//APIv3密钥
      '#description'   => $this->t('A 32 character string, Set and get it from the WeChat payment platform'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['apiV3SecretKey'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['merchantSerialNumber'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Certificate Serial Number'),//商户API证书序列号
      '#description'   => $this->t('Merchant API Certificate Serial Number'),
      '#default_value' => $this->configuration['merchantSerialNumber'],
      '#required'      => TRUE,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['merchantPrivateKey'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Private Key'),//商户下载的私钥文件内容
      '#description'   => $this->t('The contents of the merchant private key, to see apiclient_key.pem'),
      '#default_value' => $this->configuration['merchantPrivateKey'],
      '#required'      => TRUE,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['qrcolor'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('QRcode color'),
      '#description'   => $this->t('Foreground color of QR code for PC payment'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['qrcolor'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['qrbgcolor'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('QRcode background color'),
      '#description'   => $this->t('Background color of QR code for PC payment'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['qrbgcolor'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['qrsize'] = [
      '#type'          => 'number',
      '#title'         => $this->t('QRcode size'),
      '#description'   => $this->t('Size of QR code image for PC payment, in px'),
      '#step'          => 1,
      '#field_suffix'  => 'px',
      '#required'      => TRUE,
      '#default_value' => $this->configuration['qrsize'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['expires'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Expires time interval'),
      '#description'   => $this->t('Payments that exceed this time interval can not be paid, in second, default 30 days'),
      '#step'          => 1,
      '#field_suffix'  => $this->t('second'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['expires'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['systemId'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('System ID'),//系统实例ID，在同一个微信接口账号下，避免多个系统中订单号重复导致的无法付款
      '#description'   => $this->t('Only 0-9 letters,To avoid duplicate order numbers for multiple systems on the same wechat api config'),
      '#default_value' => $this->configuration['systemId'],
      '#pattern'       => '^[A-Za-z]{0,9}$',
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['appId'] = $values['appId'];
      $this->configuration['appSecretKey'] = $values['appSecretKey'];
      $this->configuration['merchantId'] = $values['merchantId'];
      $this->configuration['apiSecretKey'] = $values['apiSecretKey'];
      $this->configuration['apiV3SecretKey'] = $values['apiV3SecretKey'];
      $this->configuration['merchantSerialNumber'] = $values['merchantSerialNumber'];
      $this->configuration['merchantPrivateKey'] = str_replace("\r\n", "\n", $values['merchantPrivateKey']);
      $this->configuration['qrcolor'] = $values['qrcolor'];
      $this->configuration['qrbgcolor'] = $values['qrbgcolor'];
      $this->configuration['systemId'] = $values['systemId'];
      $this->configuration['qrsize'] = $values['qrsize'];
      $this->configuration['expires'] = $values['expires'];
      $this->configuration += $this->defaultConfiguration();
      $msg = '';
      $wechatPayCertificate = WechatAPI::getWechatCertificate($this->configuration);
      if ($wechatPayCertificate) {
        $this->configuration['wechatPayCertificate'] = $wechatPayCertificate;
        $this->configuration['wechatPayCertificateTime'] = time();
        $msg = $this->t('wechat platform certificates update successfully ! appID: @appID', ['@appID' => $this->configuration['appId']]);
        \Drupal::logger('commerce_wechat')->info($msg);
      }
      else {
        $msg = $this->t('wechat platform certificates update fail ! appID: @appID', ['@appID' => $this->configuration['appId']]);
        \Drupal::logger('commerce_wechat')->error($msg);
        $msg = $this->t("Failed to get the wechat platform certificate. Please check whether the parameters are correct");
        $this->messenger()->addError($msg);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    /**
     * 该方法在离站支付完成后返回页面中执行，具体调用是在：
     * \Drupal\commerce_payment\Controller\PaymentCheckoutController::returnPage
     * 路由为：commerce_payment.checkout.return
     * 在微信支付模块中会一并返回支付实体的ID
     * 本方法向微信服务器进行主动查询，付款失败将后退流程，成功将继续下一步
     * 不论结果如何本方法都不会去操作支付实体，这将在异步通知中进行，因为微信支付一定会进行异步通知
     */
    $paymentID = $request->query->get('payment');
    if (empty($paymentID)) {
      throw new PaymentGatewayException($this->t('On payment return page, missing payment entity ID'));
    }
    $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load((int) $paymentID);
    if (empty($payment)) {
      throw new PaymentGatewayException($this->t('On payment return page, payment entity ID: @paymentID does not exist', ['@paymentID' => $paymentID]));
    }
    if ($payment->getOrder()->id() != $order->id()) {
      //需要验证返回的支付实体属于对应订单，避免人为返回一个其他订单的已支付实体，进而攻击系统使得本订单进入下一步流程
      throw new PaymentGatewayException($this->t('On payment return page, payment entity ID:@paymentID and order ID:@orderID do not match', ['@paymentID' => $paymentID, '@orderID' => $order->id()]));
    }

    //进行主动查询
    $wechatAPI = new WechatAPI($this->getConfiguration());
    $result = $wechatAPI->query(OrderIDConverter::toWechat($paymentID, $this->getConfiguration()['systemId']));
    if ($result == FALSE) {
      throw new PaymentGatewayException($this->t('On payment return page, Payment query failed'));
    }
    if (isset($result['trade_state']) && $result['trade_state'] == 'SUCCESS') {
      //进一步检查支付金额
      $paymentTotal = \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($payment->getAmount());
      $payerTotal = $result['amount']['payer_total'] ?: 0;
      if ($paymentTotal == (int) $payerTotal) {
        //只有当这条件出现才被认为是支付成功 直接返回即可进入下一步流程
        return TRUE;
      }
    }

    /**
     * 此处你可能会考虑到几种情况：
     * 1、在PC端，用户已经扫码，但在完成支付前点击了完成按钮，系统回退显示没有付款，但之后用户又在之前的二维码上完成了付款
     * 2、腾讯服务器出现延迟，用户已支付，但查询尚未成功，导致界面提示未付款
     * 当以上情况出现时，用户实际已经付款，但浏览器显示订单未付款，且没有进入下一步，此时会发生什么呢？
     * 实际上在付款异步通知到来后，系统会自动更新订单实体的状态，包括当前所处检出流程，不管浏览器停留在什么地方都不会对后台造成影响
     * 只需要用户刷新即可，或者直接到用户中心查看。详见订单更新服务：“commerce_payment.order_updater”
     */
    $this->messenger()->addStatus($this->t('If you have paid, please check your order at the account center later'));
    throw new PaymentGatewayException($this->t('On payment return page, Payment Failed, trade_state: @trade_state', ['@trade_state' => $result['trade_state']]));
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);//你可在此处自定义取消支付提示消息
  }


  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    /**
     * 处理付款、退款的异步通知
     * 该方法将在路由commerce_payment.notify中调用，调用方法：
     * \Drupal\commerce_payment\Controller\PaymentNotificationController::notifyPage
     */
    $wechatAPI = new WechatAPI($this->getConfiguration());
    if (!$this->logger) {
      $this->logger = \Drupal::logger('commerce_wechat');
    }

    //调用接口验证签名
    if (!$wechatAPI->verifyNotify($request)) {
      //验签失败
      $this->logger->warning($this->t('Wechat Pay asynchronous notification signature verification failed !'));
      return new JsonResponse(['code' => 'ERROR', "message" => "signature verification failed"], 400);
    }

    //调用接口解密数据
    $result = $wechatAPI->decrypt($request);
    if ($result == FALSE) {
      $this->logger->warning($this->t('Failed to decrypt Wechat Pay asynchronous notification data !'));
      return new JsonResponse(['code' => 'ERROR', "message" => "decrypt failed"], 400);
    }

    //微信支付采用'event_type'标识通知类型
    //这里以此判断是付款通知还是退款通知 付款以“TRANSACTION”开始 退款以“REFUND”开始
    $notifyType = json_decode($request->getContent())->event_type;
    if (($notifyType & "TRANSACTION") === "TRANSACTION") { //性能原因不使用strpos函数
      return $this->onPayNotify($result);
    }
    elseif (($notifyType & "REFUND") === "REFUND") {
      return $this->onRefundNotify($result);
    }
    else {
      $this->logger->warning($this->t('Payment asynchronous notification of unknown type !'));
      return new JsonResponse(['code' => 'ERROR', "message" => "notification type unknown"], 400);
    }
  }

  /**
   * 处理支付异步通知
   *
   * @param array $result 微信发送的通知内容数组 已验签和解密
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function onPayNotify(array $result) {
    //只需处理付款成功的通知
    if ($result['trade_state'] != 'SUCCESS') {
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);//这样回复是避免微信进行多次通知
    }
    $paymentID = (int) OrderIDConverter::toDrupal($result['out_trade_no'], $this->configuration['systemId']);
    $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load($paymentID);
    if (empty($payment)) {
      $this->logger->warning($this->t('In pay asynchronous notifications, Payment entity(id:@id) does not exist', ['@id' => $paymentID]));
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
    }
    if (
      \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($payment->getAmount()) != (int) $result['amount']['payer_total']
      || $payment->getAmount()->getCurrencyCode() != $result['amount']['payer_currency']
    ) {
      //支付金额或币种不正确 将不处理
      $this->logger->warning($this->t('In pay asynchronous notifications, Payment entity(id:@id) amount or currency is abnormal', ['@id' => $paymentID]));
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
    }

    //现在可以判定支付已成功 开始更新支付实体
    $payment->setCompletedTime(strtotime($result['success_time']));
    $payment->setRemoteId($result['transaction_id']);
    $payment->setRemoteState($result['trade_state_desc']);
    $payment->setState('completed');
    $payment->save();
    return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
  }

  /**
   * 处理退款异步通知
   * 退款异步通知和退款主动查询均会对退款状态进行存档操作
   * 在极小概率下，为避免并发性发生争抢而出现的数据问题，必须进行锁控制
   *
   * @param array $result 微信发送的通知内容数组 已验签和解密
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @see \Drupal\commerce_wechat\Controller\QueryRefund::index
   *
   */
  protected function onRefundNotify(array $result) {
    $out_refund_no = explode('_', $result['out_refund_no']);
    $refundID = $out_refund_no[1];
    $paymentID = (int) OrderIDConverter::toDrupal($out_refund_no[0], $this->configuration['systemId']);

    $operationID = 'refund_' . $paymentID . '_' . $refundID;
    $lock = \Drupal::lock(); //得到锁对象
    if (!$lock->acquire($operationID)) {
      $this->logger->info(
        $this->t('In refund asynchronous notifications, Ignore notification because of concurrency, Refund ID(@refundID) of payment(@paymentID) is executing a refund query, then state will be updated',
          ['@refundID' => $refundID, '@paymentID' => $paymentID])
      );
      return new JsonResponse(['code' => 'ERROR', "message" => "Disable concurrent operations"], 500);
    }
    //得到了锁，可继续进行操作
    $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load($paymentID);
    if (empty($payment)) {
      $this->logger->warning($this->t('In refund asynchronous notifications, Payment entity(id:@id) does not exist', ['@id' => $paymentID]));
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
    }
    $orderEntity = $payment->getOrder();
    $moduleData = $orderEntity->getData(COMMERCE_WECHAT_DATA_KEY, []);
    if (!isset($moduleData['refund'][$paymentID][$refundID])) {
      $this->logger->warning(
        $this->t('In refund asynchronous notifications, Refund ID(@refundID) of payment(@paymentID) does not exist',
          ['@refundID' => $refundID, '@paymentID' => $paymentID])
      );
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
    }
    $refundData = $moduleData['refund'][$paymentID][$refundID];
    if ($refundData['state'] == static::REFUND_SUCCESS) { //已经退款成功的退款单不用再处理，防止微信重发通知
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
    }
    if ($result['refund_status'] == 'SUCCESS'
      && $result['amount']['payer_refund'] == $refundData['refund_amount']
    ) {
      //可认为退款成功了
      $refundData['state'] = static::REFUND_SUCCESS;
      $refundData['data'] = $result;
      $amount = \Drupal::service('commerce_price.minor_units_converter')->fromMinorUnits($refundData['refund_amount'], $refundData['currency']);
      $old_refunded_amount = $payment->getRefundedAmount();
      if (!empty($old_refunded_amount)) {
        $new_refunded_amount = $amount->add($old_refunded_amount);
      }
      else {
        $new_refunded_amount = $amount;
      }
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->state = 'partially_refunded';
      }
      else {
        $payment->state = 'refunded';
      }
      $payment->setRefundedAmount($new_refunded_amount);
      $moduleData['refund'][$paymentID][$refundID] = $refundData;
      $orderEntity->setData(COMMERCE_WECHAT_DATA_KEY, $moduleData);
      $orderEntity->save();
      $payment->save();
    }
    else {
      $refundData['state'] = static::REFUND_FAIL;
      $refundData['data'] = $result;
      $moduleData['refund'][$paymentID][$refundID] = $refundData;
      $orderEntity->setData(COMMERCE_WECHAT_DATA_KEY, $moduleData);
      $orderEntity->save();
      //恢复支付实体的状态，否则如果一直挂起，那么无法重新发起退款
      $refundedAmount = $payment->getRefundedAmount();
      if (empty($refundedAmount) || $refundedAmount->isZero()) {
        $payment->state = 'completed';
      }
      elseif ($refundedAmount->lessThan($payment->getAmount())) {
        $payment->state = 'partially_refunded';
      }
      else {
        $payment->state = 'refunded';
      }
      $payment->save();
    }
    $lock->release($operationID);
    //脚本结束会自动释放 但当操作结束后应当明确释放锁，依赖锁自动过期有性能损耗
    return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
  }


  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    /**
     * 支付网关插件实例化时会在以下方法中补充设置本插件的操作表单释文（故查看缓存的插件定义无法看到）：
     * \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase::getDefaultForms
     * 如果实现了退款接口，那么会在释文的表单项中补充以下系统默认的退款操作表单：
     * 操作名：refund-payment，表单类：Drupal\commerce_payment\PluginForm\PaymentRefundForm
     */
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);
    $config = $this->getConfiguration();//插件配置，即微信API接口配置
    $orderEntity = $payment->getOrder(); //订单实体
    if (!$this->logger) {
      $this->logger = \Drupal::logger('commerce_wechat');
    }
    $wechatAPI = new WechatAPI($config, $this->logger);
    $paymentID = $payment->id();

    /**
     * 构建退款订单数组：
     */
    $time = (string) time();//用作退款ID标识
    $order['order_number'] = OrderIDConverter::toWechat($paymentID, $config['systemId']);
    //原支付订单号
    $order['refund_number'] = $order['order_number'] . '_' . $time;
    //退款单号  String  最大长度64
    $order['refund_amount'] = \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($amount);
    //退款金额
    $order['total'] = \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($payment->getAmount());
    //原支付总金额
    $order['currency'] = $payment->getAmount()->getCurrencyCode();
    //货币代码
    $refund_reason = $this->t('Refund') . ':';
    if (isset($orderEntity->getItems()[0])) {
      $refund_reason .= Unicode::truncate($orderEntity->getItems()[0]->getTitle(), 20, FALSE, TRUE);
    }
    $order['refund_reason'] = $refund_reason . $this->t('OrderID') . ':' . $orderEntity->id();
    $order['notify_url'] = $this->getNotifyUrl()->toString(FALSE);

    //退款信息初始化数据
    $refundData = [
      'state'         => static::REFUND_NEW,
      'refund_amount' => $order['refund_amount'], //注意这里是以分为单位保存
      'currency'      => $order['currency'],
      'data'          => [], //用于保存微信返回的数据，以便查询
    ];
    $moduleData = $orderEntity->getData(COMMERCE_WECHAT_DATA_KEY, []);
    if (!isset($moduleData['refund'][$paymentID])) {
      $moduleData['refund'][$paymentID] = [];
    }
    $moduleData['refund'][$paymentID][$time] = $refundData;
    $orderEntity->setData(COMMERCE_WECHAT_DATA_KEY, $moduleData);
    $orderEntity->save();
    $result = $wechatAPI->refund($order);
    if ($result == FALSE) {
      $this->logger->warning('Wechat Pay failed to refund');
      throw new PaymentGatewayException($this->t('Wechat Pay failed to refund'));
    }
    $payment->state = 'pending'; //当状态设置为挂起时退款按钮将不可用
    $payment->save();
    //不必处理微信返回数据，也不反应到支付实体中，这些在异步通知中执行更加可靠，这里仅需更新状态为挂起即可
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    $payment_state = $payment->getState()->getId();
    return in_array($payment_state, ['completed', 'partially_refunded']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->getId();
    $operations = [];
    $operations['refund'] = [
      'title'       => $this->t('Refund'),
      'page_title'  => $this->t('Refund payment'),
      'plugin_form' => 'refund-payment',
      'access'      => $this->canRefundPayment($payment),
    ];
    $operations['view_refund'] = [
      'title'       => $this->t('View refund'),
      'page_title'  => $this->t('View refund Details'),
      'plugin_form' => 'view-refund',
      'access'      => in_array($payment_state, ['pending', 'partially_refunded', 'refunded']),
    ];
    return $operations;
  }


}

<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180613
 */

namespace Drupal\yunke_help\Controller;


/**
 * 自定义控制器
 * 清空数据库缓存表，这和系统后台的缓存清理有区别，该操作更加彻底
 *
 * @package Drupal\yunke_help\Controller
 */
class CacheClean 
{
    
    public function clean()
    {
        $db = \Drupal::database();
        $cacheTables =$db->schema()->findTables("cache_%");
        /*
        $cacheTables = [
            "cachetags",
            "cache_bootstrap",
            "cache_config",
            "cache_container",
            "cache_data",
            "cache_default",
            "cache_discovery",
            "cache_dynamic_page_cache",
            "cache_entity",
            "cache_menu",
            "cache_page",
            "cache_render",
            "cache_toolbar",
        ];
        */
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . "   \n";
        echo "该功能清空所有表名以'cache'开始的表，请开发者遵循最佳实践，非缓存表不能以此开头\n\n";
        foreach ($cacheTables as $table) {
            $db->truncate($table)->execute();
            echo "已清空数据表:".$table."\n";
        }
        echo "\n缓存清空操作已完成 by:yunke_help模块\n";
        echo "</pre>\n";
        die;
    }
    
}


<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180610
 */

namespace Drupal\yunke_help\Controller;

use Drupal\Core\Serialization\Yaml as drupalYaml;

/**
 * 用于yaml数据的编码解码
 * 如果你对yaml编写不熟悉 或者避免不必要的错误 可以使用该类
 * Class Test
 *
 * @package Drupal\yunke_help\Controller
 */
class Yaml
{
  /**
   * 系统根目录的绝对路径 不带后缀斜杠 如：C:\root\drupal
   *
   * @var string
   */
  protected $root;

  /**
   * 本模块相对于系统根目录的路径，不带前后缀斜杠
   *
   * @var string
   */
  protected $yunke_help_path;

  public function __construct()
  {
    $this->root = \Drupal::service("app.root");
    $this->yunke_help_path = \Drupal::moduleHandler()->getModule("yunke_help")->getPath();
  }

  /**
   * 编码yaml
   *
   */
  public function encode()
  {
    /**
     * 可以在此输入你的php数据，它将被编码从yaml格式
     */

    $data = [
      "str"                => "字符串",
      "str_with_space"     => "yunke 云客",
      "str_with_quotation" => "'' \"\"",
      "str_with_slash"     => "\\/",
      "str_with_brace"     => "{}[]",
      "str_with_colon"     => ":",
      "str_null"           => "",
      "str_newline"        => "yunke\nyunke",
      "int"                => 123,
      "int_str"            => "123",
      "float"              => 3.14,
      "bool_true"          => true,
      "bool_str"           => "true",
      "null"               => NULL,
      "arr"                => ["one", "two", "three"],
      "arr_key"            => ["a" => "one", "b" => "two", "c" => "three"],

    ];
    $data_yaml = drupalYaml::encode($data);
    $file = $this->root . "/" . $this->yunke_help_path . "/data/yunkeyaml.yml";
    file_put_contents($file, $data_yaml);
    echo "<pre>";
    echo "请在文件：" . $this->root . "/" . $this->yunke_help_path . "/src/Controller/Yaml.php的encode方法中指定要编码的php数据\n";
    echo "编码后的yaml数据被保存在：" . $file . "中，内容如下：\n\n\n";
    print_r($data_yaml);
    echo "\n\n原php数据如下：\n\n";
    print_r($data);
    echo "\n </pre>";
    die;
  }

  /**
   * 解码yaml
   *
   */
  public function decode()
  {
    /**
     * 你可以在这里指定一个yaml文件进行解码，观察编码的yaml文件格式是否正确
     */
    $filename = $this->root . "/" . $this->yunke_help_path . "/yunke_help.routing.yml";

    //解码任意yaml文件，测试编码错误
    $data = drupalYaml::decode(file_get_contents($filename));

    //也可以使用如下代码专门针对info.yml文件测试，除编码格式外，还会检查必须项，注意传入的是绝对路径，而不是文件内容
    //$data=\Drupal::service("info_parser")->parse($filename);

    echo "<pre> \n";
    echo "你可以在文件：" . $this->root . "/" . $this->yunke_help_path . "/src/Controller/Yaml.php的decode方法中指定需要解码的yaml文件\n";
    echo "当前解码的yaml文件是：" . $filename . " 解码后的内容如下：\n\n";
    print_r($data);
    echo "\n </pre>";
    die;
  }


}


<?php
/**
 * @file
 * 多步表单（multi-step forms）示例
 * by:yunke 【云客：云游天下，做客四方】
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * 该表单让用户通过多个步骤提交一些数，然后对这些数求和，
 * 演示多步表单（multi-step forms）的提交过程
 */
class MultiStepForm extends FormBase
{

  protected $data = []; //保存每一步提交的数据
  protected $maxStep = 3; //设置最大提交步数
  protected $currentStep = 1; //保存当前处于哪一步中

  public function getFormId()
  {
    return 'yunke_help_form_multi_step_Form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['#title'] = '提交一些数求和';
    $form['num'] = array(
      '#type'          => 'number',
      '#title'         => '随意输入一个数',
      '#default_value' => false,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    );
    if ($this->data) {
      $num = count($this->data);
      $form['msg'] = ['#markup' => '已提交次数：' . $num];
      $form['msg']['#markup'] .= '<br>剩余提交次数：' . ($this->maxStep - $num);
      $form['msg']['#markup'] .= '<br>历次输入为：' . implode(', ', $this->data);
    }
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type'        => 'submit',
      '#value'       => "提交",
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    if ($this->currentStep < $this->maxStep) {
      $form_state->setRebuild(); //这是多步表单的关键
      $this->data[] = $form_state->getValue($form['num']['#parents']);
      //当再次呈现表单时会默认填入上一步的输入，如果需要改变用户的输入，可使用以下语句：
      \Drupal\Component\Utility\NestedArray::setValue($form_state->getUserInput(), $form['num']['#parents'], null);
      //试图通过以下方式是无效的，因为后续流程会重置他们
      //$form_state->setValue($form['text']['#parents'], $input);
      //$form['text']['#value'] = $input;
      $this->messenger()->addMessage("已提交:{$this->currentStep}次");
      $this->currentStep++;
      return;
    }
    $this->messenger()->addMessage('输入完成');
    $this->data[] = $form_state->getValue($form['num']['#parents']);
    $msg = "输入次数:" . $this->maxStep;
    $msg .= "<br>历次输入为:" . implode(', ', $this->data);
    $msg .= "<br>和是：" . array_sum($this->data);
    $return = ['#markup' => $msg, '#title' => $form['#title']];

    //由于提交处理器不能直接返回渲染数组，下面将其转化为响应对象
    $kernel = \Drupal::service('http_kernel.basic');
    $request = \Drupal::request();
    $type = \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST;
    $event = new \Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent($kernel, $request, $type, $return);
    \Drupal::service('event_dispatcher')->dispatch(\Symfony\Component\HttpKernel\KernelEvents::VIEW, $event);
    $response = $event->getResponse();
    $form_state->setResponse($response);
  }
}

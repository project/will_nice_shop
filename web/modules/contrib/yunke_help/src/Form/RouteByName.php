<?php

/**
 * 根据路由名查看路由定义
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RouteByName extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_route_by_name';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#title'] = '显示路由定义';
        $form['route_name'] = array(
            '#type'  => 'textfield',
            '#title' => "输入路由名：",
            '#size'  => 100,
            '#required' => TRUE,
        );
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $route_name = $form_state->getValue('route_name');
        if (empty(trim($route_name))) {
            $form_state->setErrorByName('route_name', "路由名不能为空");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $route_name = $form_state->getValue('route_name');
        try{
            $route = \Drupal::service('router.route_provider')->getRouteByName($route_name);
        }catch (\Exception $e){
            $route="异常：可能是路由名不存在";
        }
        
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的路由名是：\n" . $route_name . "\n\n";
        echo "路由定义如下：\n";
        print_r($route);
        echo "\n</pre>";
        die;
    }

}

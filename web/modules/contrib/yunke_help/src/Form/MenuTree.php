<?php

/**
 * 显示菜单树
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class MenuTree extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_menuTree';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $menuNames = [];
        $entities = \Drupal::entityTypeManager()->getStorage('menu')->loadMultipleOverrideFree();
        foreach ($entities as $entity) {
            $menuNames[$entity->id()] = $entity->label();
        }

        $form['description'] = [
            '#markup' => '通过程序或yml文件定义菜单时，确定菜单位置需要知道父菜单的id，本功能打印菜单树的结构，将显示菜单id、路由、类型等信息'
        ];
        $form['menuName'] = [
            '#type'         => 'select',
            //'#title'        => '选择菜单：',
            '#options'      => $menuNames,
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择菜单：',
        ];
        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['submit'] = array(
            '#type'  => 'submit',
            '#value' => '查看',
        );
        $form['actions']['reset'] = [
            '#type'        => 'button',
            '#button_type' => 'reset',
            '#value'       => $this->t('Reset'),
            '#attributes'  => [
                'onclick' => 'this.form.reset(); return false;',
            ],
        ];
        $form['#attributes']['target'] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        $form['#title'] = "显示菜单树信息";

        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $menuName = $form_state->getValue('menuName');
        if (empty(trim($menuName))) {
            $form_state->setErrorByName('menuName', "请选择一个菜单");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $menuName = $form_state->getValue('menuName');
        $menu_tree = \Drupal::service("menu.link_tree");
        $parameters = new \Drupal\Core\Menu\MenuTreeParameters();
        $parameters->setMinDepth(1)->setMaxDepth(9);
        $tree = $menu_tree->load($menuName, $parameters);
        $tree = $this->printMenuTree($tree);

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的菜单是：" . $menuName . "\n\n";
        echo "菜单树结构及信息如下：\n";
        print_r($tree);
        echo "\n</pre>";
        die;
    }

    /**
     * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
     *
     * @return array
     */
    protected function printMenuTree($tree = [])
    {
        $menuTree = [];
        foreach ($tree as $key => $menuLinkTreeElement) {
            $link = $menuLinkTreeElement->link;
            $menuTree[$key]['id'] = $link->getPluginId();
            $menuTree[$key]['title'] = (string)$link->getTitle();
            $menuTree[$key]['provider'] = $link->getProvider();
            $menuTree[$key]['routeName'] = $link->getRouteName();
            if ($link instanceof \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent) {
                $menuTree[$key]['type'] = '通过后台菜单管理界面动态定义的菜单';
            } elseif ($link instanceof \Drupal\Core\Menu\MenuLinkDefault) {
                $menuTree[$key]['type'] = "通过yml文件静态定义的菜单";
            } else {
            }
            if ($menuLinkTreeElement->subtree) {
                $menuTree[$key]['subMenu'] = $this->printMenuTree($menuLinkTreeElement->subtree);
            }
        }
        return $menuTree;
    }

}

<?php

/**
 * 查看视图配置数据
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ViewConfig extends FormBase
{

  public function getFormId()
  {
    return 'yunke_help_view_config';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if (!\Drupal::moduleHandler()->moduleExists('views')) {
      return ['#markup' => "视图模块未开启",];
    }
    $views = \Drupal::entityTypeManager()->getStorage('view')->loadMultiple();
    foreach ($views as $k => $v) {
      if ($v->status()) {
        $views[$k] = $k . '(' . $v->label() . ')';
      } else {
        $views[$k] = '【未启用】' . $k . '(' . $v->label() . ')';
      }
    }
    $form['#title'] = '查看视图配置数据';
    $form['view'] = [
      '#type'         => 'select',
      '#options'      => $views,
      '#empty_option' => $this->t('-select-'),
      '#field_prefix' => '选择视图：',
      '#required'     => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type'        => 'submit',
      '#value'       => "提交",
      '#button_type' => 'primary',
    );
    $form['actions']['reset'] = array(
      '#type'       => 'html_tag',
      '#tag'        => 'input',
      '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
    );
    $form['#attributes']["target"] = "_blank";
    $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $view = $form_state->getValue('view');
    if (empty(trim($view))) {
      $form_state->setErrorByName('view', "视图不能为空");
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $view = $form_state->getValue('view');
    $viewEntity = \Drupal::entityTypeManager()->getStorage('view')->load($view);
    $view_config = $viewEntity->toArray();
    echo "<pre>\n";
    echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
    echo "当前指定的视图是：" . $view . "(" . $viewEntity->label() . ")" . "\n\n";
    echo "视图配置数据如下：\n";
    print_r($view_config);
    echo "\n</pre>";
    die;
  }

}

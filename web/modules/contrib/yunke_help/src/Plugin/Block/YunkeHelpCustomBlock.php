<?php

namespace Drupal\yunke_help\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * 提供一个完全自定义的块
 * “块”是drupal中特别重要的内容，页面所有信息都以块的方式呈现，可以说知道如何自定义块的话就能开始制作简单的网站了
 * 因此本模块提供了一个通用示例
 *
 * @Block(
 *   id = "yunke_help_custom_block",
 *   admin_label = @Translation("yunke help 模块自定义块admin_label"),
 *   label = "yunke help 块label"
 *
 * )
 */
class YunkeHelpCustomBlock extends BlockBase
{

    /**
     * 定义默认配置
     * 块插件的配置由基本配置、默认配置、实例化时传入的配置合并而成，越靠后优先级越高
     * 在本示例中当块第一次配置时，使用配置系统中的值，在以后配置时将使用块配置实体中的值
     * 也就是实例化时传入的值，一个块插件可以实例化出多个块，因此才能实现多个分区都放置
     * 每个实例使用自己在配置实体中的值，但第一次配置时共享默认配置
     */
    public function defaultConfiguration()
    {
        $default_config = \Drupal::config('yunke_help.block');
        return [
            'label_display' => FALSE,
            'title'         => $default_config->get('custom_block.title'),
            'content'       => $default_config->get('custom_block.content'),
        ];
    }

    /**
     * 构建块内容 返回这个块的渲染数组
     * 通过渲染数组任意控制外观 可以直接写标签也可以通过主题钩子去调用模板
     * 这里是一个简单的列子，在真实使用中通常需要判断权限控制、分区等等信息后作出不一样的显示
     */
    public function build()
    {
        $config = $this->getConfiguration();
        $elements = [];
        $elements["title"] = [
            '#type'   => "markup",
            '#markup' => "<div>{$config['title']}</div>",
        ];
        //示例通过两种方式去实现外观控制
        $elements["title"] = [
            '#theme' => "page_title",
            '#title' => $config['title'],
        ];
        $elements["content"] = [
            '#type'   => "markup",
            '#markup' => $config['content'],
        ];
        return $elements;
    }

    /**
     * 判断在页面中本块是否可访问，返回对象，如果不可访问将不会出现 如果不覆写该方法将默认可访问
     * @param \Drupal\Core\Session\AccountInterface $account
     * @return AccessResult|\Drupal\Core\Access\AccessResultAllowed
     */
    protected function blockAccess(AccountInterface $account) {
        return AccessResult::allowed();
    }

    /**
     * 在后台/admin/structure/block添加和配置本块时提供配置表单
     * 显示一个标题（普通输入控件）和一个富文本框用于编辑页面内容
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);

        $config = $this->getConfiguration(); //默认值将来自块配置实体

        $form['title'] = array(
            '#type'          => 'textfield',
            '#title'         => "输入块标题",
            '#description'   => $this->t('将显示到块的标题位置'),
            '#default_value' => isset($config['title']) ? $config['title'] : '',
        );
        $form['content'] = array(
            '#type'          => 'text_format',
            '#title'         => "输入块内容",
            '#description'   => $this->t('将显示到块的内容位置'),
            '#format'        => 'full_html',
            '#default_value' => isset($config['content']) ? $config['content'] : '',
        );

        return $form;
    }

    /**
     * 验证配置表单
     * @param array              $form
     * @param FormStateInterface $form_state
     */
    public function blockValidate($form, FormStateInterface $form_state)
    {
        if (empty($form_state->getValue('title'))) {
            $form_state->setErrorByName('title', "标题不能为空");
        }
        if (empty($form_state->getValue('content')['value'])) {
            $form_state->setErrorByName('content', "内容不能为空");
        }
    }

    /**
     * 保存配置信息
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        $this->setConfigurationValue('title', $form_state->getValue('title'));
        $this->setConfigurationValue('content', $form_state->getValue('content')['value']);
        
        /*
         * 以下部分通常不需要，同一个块插件可以实例化多个块分别放在多个页面分区中，每个具体的块实例有其独立的配置
         * 独立的配置保存在块配置实体中
         * 以下部分在本示例中只是修改了初始值而已
         */
        $configEditable = \Drupal::configFactory()->getEditable('yunke_help.block');
        $configEditable->set('custom_block.title', $form_state->getValue('title'));
        $configEditable->set('custom_block.content', $form_state->getValue('content')['value']);
        $configEditable->save();
    }

}

<?php

/**
 * 本文件为云客帮助模块提供的工具函数库，工具函数用于调试等目的，并非钩子函数
 * 本函数库通过请求事件订阅器加载 在此时机之前如需使用需要手动加载
 * BY:云客【云游天下，做客四方】,微信号：php-world，讨论请加qq群203286137
 */


/**
 *
 * 注意：这并非修改钩子函数，而是工具函数
 * 用户经常使用钩子修改表单的渲染数组，但很多时候并不知道表单的结构，在没有断点工具时，如果直接打印表单变量
 * 会由于内容太大、内容间存在循环引用等原因卡死系统，本函数用于显示数组的结构，使用如下：
 *
 * @code
 *   $node = \Drupal::entityTypeManager()->getStorage('node')->load(1);
 *   $nodeForm = \Drupal::service('entity.form_builder')->getForm($node);
 *   yunke_help_tool_array_print_alter($nodeForm, 10);
 *   print_r($nodeForm);die;
 * @endcode
 *
 * 超过最大显示深度的子数组会被忽略，使用“Max depth limit”标注，变量间引用通过“reference: $array”标注，但需要注意本
 * 函数用于判断是否引用仅通过值是否全等为依据，并不一定能反应出代码的&写法，这在查看数组结构时通常已经足够了
 *
 * @param        $var         要显示的数组，修改成适合打印的值
 * @param int    $maxDeep     遍历显示的最大深度
 * @param int    $currentDeep 仅内部使用
 * @param array  $flat        仅内部使用
 * @param string $path        仅内部使用
 */
function yunke_help_tool_array_print_alter(&$var, $maxDeep = 8, $currentDeep = 0, &$flat = [], $path = '')
{
    if ($maxDeep < 0) {
        throw new \InvalidArgumentException('$maxDeep must >=0 , Be given:' . $maxDeep);
    }
    $currentDeep++;
    if ($currentDeep > $maxDeep) {
        $var = 'Max depth limit(' . $currentDeep . ')...';
        return;
    }
    if ($path === '') {
        $flat[''] =& $var;
    }
    if (is_array($var)) {
        foreach ($var as $key => &$value) {
            if (($flatKey = array_search($value, $flat, true)) !== false) {
                unset($var[$key]);
                $var[$key] = 'reference: $array' . $flatKey;
                continue;
            }
            $currentPath = $path . (is_numeric($key) ? "[{$key}]" : "['{$key}']");
            $flat[$currentPath] =& $value;
            if (is_object($value)) {
                $var[$key] = 'objectClass:' . get_class($value);
            } elseif (is_array($value)) {
                yunke_help_tool_array_print_alter($var[$key], $maxDeep, $currentDeep, $flat, $currentPath);
            }
        }
    } else {
        return;
    }
}

/**
 * 打印大数组，防止内存耗尽
 *
 * @code
 *   $node = \Drupal::entityTypeManager()->getStorage('node')->load(1);
 *   $nodeForm = \Drupal::service('entity.form_builder')->getForm($node);
 *   yunke_help_tool_array_print($nodeForm, 10);
 *   die;
 * @endcode
 *
 * @param        $var         要显示的数组
 * @param int    $maxDeep     遍历显示的最大深度
 */
function yunke_help_tool_array_print($var, $maxDeep = 20)
{
    yunke_help_tool_array_print_alter($var, $maxDeep);
    print_r($var);
}

/**
 * @deprecated 弃用，为兼容历史版本而存在，用yunke_help_tool_array_print_alter代替
 */
function yunke_help_tool_form_print(&$var, $maxDeep = 8, $currentDeep = 0, &$flat = [], $path = '')
{
    yunke_help_tool_array_print_alter($var, $maxDeep, $currentDeep, $flat, $path);
}

<?php
/**
 * 未来很美（will-nice)统一收银系统用户协议
 */

namespace Drupal\will_nice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ProtocolForm extends FormBase {

  public function getFormId() {
    return 'will_nice_protocol_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = '用户协议';
    $form['protocolText'] = [
      '#type'          => 'textarea',
      '#title'         => '',
      '#rows'          => 20,
      '#default_value' => file_get_contents('profiles/will_nice/protocol.txt'),
      '#attributes'    => [
        'autocomplete' => 'off',
        'readonly'     => TRUE,
      ],
    ];
    $form['agree'] = [
      '#type'          => 'checkbox',
      '#title'         => '选中本项表示你同意本协议（否则请关闭本页）',
      '#required'      => TRUE,
      '#return_value'  => TRUE,
      '#default_value' => FALSE,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => '同意并继续',
      '#button_type' => 'primary',
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $agree = $form_state->getValue('agree');
    if (empty($agree)) {
      $form_state->setError($form['agree'], '须同意协议才能继续');
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $build_info['args'][0]['parameters']['agree'] = 1;
    $form_state->setBuildInfo($build_info);
  }

}
